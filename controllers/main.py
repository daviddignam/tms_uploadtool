# -*- coding: utf-8 -*-
from openerp import http
import requests
from werkzeug.exceptions import HTTPException, NotFound, abort
from openerp.http import Response 
from openerp.http import local_redirect, request
import json
import logging

_logger = logging.getLogger(__name__)

class uploadTool(http.Controller):

	@http.route('/uploadtool/', auth='public', methods=['GET'], website=True)
	def load(self, **kw):
		languages = http.request.env['tms.languages'].sudo().search([])
		_logger.debug("number of languages: " + str(len(languages)))
		settings = http.request.env['tms.uploadtool.settings'].sudo().browse(1)
		
		return http.request.render('tms_uploadtool.upload_form', {'tlanguages':languages, 'settings':settings})
		
	
	@http.route('/uploadtool/upload_file/', type='http', auth='public', methods=['POST'], csrf=False)
	def save_file(self, **kw):
		_logger.debug("form data file is: " + str(kw))
		file = kw.get('file_datas')
		mimetype = kw.get('mimetype')
		languages = http.request.env['tms.languages'].sudo().search([])
		valid_file, extension = self.validate_file(file, mimetype)
		if valid_file:
			_logger.debug("extension: " + str(extension))
			row_data = {
				'origin_filetype': extension.id,
				'filename': file.filename,
				'file_blob': file.read().encode('base64'),
			}
			
			row = http.request.env['tms.uploadtool.request.row'].sudo().create(row_data)
			
			row_html = http.request.render('tms_uploadtool.upload_request_row', {'tlanguages':languages, 'record':row})
			
			return row_html.render()
		else:
			settings = http.request.env['tms.uploadtool.settings'].sudo().browse(1)
			error_html = http.request.render('tms_uploadtool.upload_file_error', {'filename':file.filename,'extension':extension,'settings':settings})
			_logger.debug("file upload error: " + str(error_html.render()))
			return Response(error_html.render(), status=415)
		
	def validate_file(self, file, mimetype):
		settings = http.request.env['tms.uploadtool.settings'].sudo().browse(1)
		file_extension = file.filename.split('.')[-1]
		# _logger.debug("extension: " + str(file_extension) + " mimetype: " + str(mimetype))
		# _logger.debug("setting ext: " + str(settings.accepted_extensions) + " mime: " + str(settings.accepted_mimetypes))
		valid = False
		if file_extension in settings.accepted_extensions and mimetype in settings.accepted_mimetypes:
			valid = True
			file_extension = http.request.env['tms.uploadtool.filetype'].sudo().search([('extension','=',file_extension)])
		_logger.debug("valid: " + str(valid))
		return valid, file_extension
	
	@http.route('/uploadtool/delete_file/', type='http', auth='public', methods=['POST'], csrf=False)
	def delete_file(self, **kw):
		record_id = kw.get('record_id')
		record = http.request.env['tms.uploadtool.request.row'].sudo().search([('id','=',record_id)])
		if record:
			success = record.sudo().unlink()
		else:
			success = False
			
		return Response(str(success), status=200)
	
	@http.route('/uploadtool/submit/', type="http", auth="public", methods=['POST'], csrf=False)
	def submit(self, **kw):
		try:
			form = http.request.httprequest.data
			_logger.debug("submitted form data: " + str(kw))
			_logger.debug("form: " + str(form))
			data = json.loads(form)
			
			temp_name = http.request.env['ir.sequence'].sudo().next_by_code('tms.uploadtool.request')
			
			request_data = {
				'name': temp_name,
				'fname': data['fname'],
				'lname': data['lname'],
				'phone': data['phone'],
				'email': data['email'],
				'comments': data['comments'],
			}
			
			request_record = http.request.env['tms.uploadtool.request'].sudo().create(request_data)
			
			_logger.debug("docs: " + str(data['docs']))
			for doc in data['docs']:
				_logger.debug("doc: " + str(doc))
				id = doc.get('id')
				record = http.request.env['tms.uploadtool.request.row'].sudo().search([('id','=',id)])
				_logger.debug("record: " + str(record))
				doc_data = {
					'parent_request': request_record.id,
					'source': doc.get('source'),
					'target': [(6,0,[int(id) for id in doc.get('target')])],
				}
				record.sudo().write(doc_data)
			
			success_html = http.request.render('tms_uploadtool.upload_submit_success', {'customer':data['fname'], 'request_name': request_record.name})
			return Response(success_html.render(), status=200)
		except Exception:
			_logger.debug("UploadTool Error processing submitted request")
			fail_html = http.request.render('tms_uploadtool.upload_fail_success',{})
			return Response(fail_html.render(), status=500)

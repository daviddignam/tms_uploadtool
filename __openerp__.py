# -*- coding: utf-8 -*-
{
    'name': "Translation Management System UploadTool",
	'summary': """
        Allows for upload of documents to be translated which results in creation of quotation for translation of said documents.""",
		
	'author': "David Dignam",
	'version': '0.1',
	
	'depends': ['tms_res','web','sale'],
	
	'data': [
		'views/assets.xml',
		'views/upload_form.xml',
		'views/upload_settings.xml',
		'views/upload_records.xml',
		'data/filetypes.xml',
		'data/request_sequence.xml',
	],
	
	'demo':[
	],
}
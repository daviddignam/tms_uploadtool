# -*- coding: utf-8 -*-

from openerp import models, fields, api
import logging
from openerp.exceptions import Warning


_logger = logging.getLogger(__name__)

class uploadToolAllowedFiles(models.Model):
	_name = 'tms.uploadtool.settings'
	
	allowed_files = fields.Many2many(comodel_name='tms.uploadtool.filetype')
	accepted_extensions = fields.Char()
	accepted_mimetypes = fields.Char()
	
	# @api.depends('allowed_files')
	# def _gen_accepted_file_str(self):
		# extension_str = ''
		# mimetype_str = ''
		# for filetype in self.allowed_files:
			# extension_str += '.' + filetype.extesion + ','
			# mimetype_str += ',' + filetype.mimetype
		# _logger.debug("extensions: " + extension_str)
		# _logger.debug("mimetypes: " + mimetype_str)
		# self.accepted_files_str  = extension_str + mimetype_str
# -*- coding: utf-8 -*-

from openerp import models, fields, api
import logging
from openerp.exceptions import Warning


_logger = logging.getLogger(__name__)

class uploadToolAllowedFilesConfig(models.TransientModel):
	_inherit = 'res.config.settings'
	_name = 'tms.uploadtool.config'
	
	allowed_files = fields.Many2many(comodel_name='tms.uploadtool.filetype')
	accepted_extensions = fields.Char()
	accepted_mimetypes = fields.Char()
	
	@api.model
	def get_default_allowed_files(self, fields):
		try:
			uploadtool = self.env['tms.uploadtool.settings'].browse(1)
			_logger.debug("check onload: " + str(uploadtool))
			if uploadtool.exists():
				allowed_files = [(6,0,[filetype.id for filetype in uploadtool.allowed_files])]
				accepted_extensions = uploadtool.accepted_extensions
				accepted_mimetypes = uploadtool.accepted_mimetypes
				_logger.debug("uploadtool exists: " + str(allowed_files))
			else:
				# pdf = self.env.ref('tms_upload.uploadtool_pdf').id
				pdf = self.get_file_record('uploadtool_pdf')
				word = self.get_file_record('uploadtool_word')
				excel = self.get_file_record('uploadtool_excel')
				powerpoint = self.get_file_record('uploadtool_excel')
				jpeg = self.get_file_record('uploadtool_jpeg')
				png = self.get_file_record('uploadtool_png')
				gif = self.get_file_record('uploadtool_gif')
				html = self.get_file_record('uploadtool_html')
				text = self.get_file_record('uploadtool_text')
				allowed_files = [(6,0,[pdf.id,word.id,excel.id,powerpoint.id,jpeg.id,png.id,gif.id,html.id,text.id])]
				
			data = {
				'allowed_files': allowed_files,
				'accepted_extensions': accepted_extensions,
				'accepted_mimetypes': accepted_mimetypes,
			}
			
			if not uploadtool.exists():
				res = self.env['tms.uploadtool.settings'].create(data)
				_logger.debug("creation result: " + str(res))
			_logger.debug("returning data: " + str(data))
			return data
		except Exception:
			_logger.error("Error getting default uploadtool settings", exc_info=True)
	
	@api.onchange('allowed_files')
	def _gen_accepted_file_str(self):
		try:
			extension_str = ''
			mimetype_str = ''
			for filetype in self.allowed_files:
				extension_str += filetype.extension + ','
				mimetype_str += filetype.mimetype + ','
			_logger.debug("extensions: " + extension_str)
			_logger.debug("mimetypes: " + mimetype_str)
			self.accepted_extensions = extension_str
			self.accepted_mimetypes = mimetype_str
		except Exception:
			_logger.debug("Error onchange", exc_info=True)
	
	def get_file_record(self, file_xmlid):
		res_model, res_id = self.env['ir.model.data'].get_object_reference('tms_uploadtool',file_xmlid)
		filetype = self.env[res_model].browse(res_id)
		return filetype
		
		
	@api.multi
	def set_uploadtool_values(self):
		uploadtool = self.env['tms.uploadtool.settings'].browse(1)
		_logger.debug("uploadtool: " + str(uploadtool))
		try:
			self._gen_accepted_file_str()
			_logger.debug("self: " + str(self.accepted_extensions) + " " + str(self.accepted_mimetypes))
			data = {
				'allowed_files': [(6,0,[filetype.id for filetype in self.allowed_files])],
				'accepted_extensions': self.accepted_extensions,
				'accepted_mimetypes': self.accepted_mimetypes,
			}
			_logger.debug("check before write: " + str(uploadtool))
			if uploadtool.exists():
				uploadtool.write(data)
				_logger.debug("wrote: " + str(uploadtool.accepted_extensions) + " mime: " + str(uploadtool.accepted_mimetypes))
			else:
				self.env['tms.uploadtool.settings'].create(data)
		except Exception:
			_logger.error("Error setting uploadtool values", exc_info=True)

class uploadToolFileTypes(models.Model):
	_name = 'tms.uploadtool.filetype'
	
	name = fields.Char()
	extension = fields.Char()
	mimetype = fields.Char()
	ocr_required = fields.Boolean(string="OCR required", help="Whether the file type needs to be submitted for Optical Character Recognition before entering the Translation management System")
	

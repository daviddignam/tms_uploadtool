# -*- coding: utf-8 -*-

from openerp import api, fields, models, _
from openerp.exceptions import UserError, AccessError

import logging
_logger = logging.getLogger(__name__)

class uploadToolRequest(models.Model):
	_name = 'tms.uploadtool.request'
	
	state = fields.Selection([('draft','Not yet Submitted'),('ocr','OCR - Submitted'),('tms','TMS - Submitted'),('complete','Quotation Created'),('error_ocr','OCR Error'),('tms_error','TMS Error')], default="draft")
	name = fields.Char(string="Quotation Number", help="Will be Project Name if request processes to Quotation")
	#TODO: Check if customer details match an existing contact and assign or create new contact on request submission
	customer_record = fields.Many2one(comodel_name='res.partner')
	fname = fields.Char(string="First Name")
	lname = fields.Char(string="Last Name")
	phone = fields.Char(string="Telephone Number")
	email = fields.Char(string="Email")
	comments = fields.Text(string="Comments")
	
	rows = fields.One2many(comodel_name='tms.uploadtool.request.row', inverse_name='parent_request')
	
class UploadToolRequestRow(models.Model):
	_name = 'tms.uploadtool.request.row'
	
	parent_request = fields.Many2one(comodel_name='tms.uploadtool.request', string="Parent UploadTool Request", help="The submitted Quotation Request these documents are attached to.")
	filename = fields.Char()
	file_blob = fields.Binary(string="Original File")
	origin_filetype = fields.Many2one(comodel_name='tms.uploadtool.filetype', string="Original File Type")
	converted_file_blob = fields.Binary(string="Converted File")
	source = fields.Many2one(comodel_name='tms.languages')
	target = fields.Many2many(comodel_name='tms.languages')
	
	
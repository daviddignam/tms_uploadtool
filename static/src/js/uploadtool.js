odoo.define('web.uploadtool', function(require) {
	'use strict';

	var website = require('website.website');
	
	//Set up jquery validation on document ready
	$(document).ready(function() {
		console.log("UT: " + document.getElementById("upload_tool"));
		if (document.getElementById("upload_tool")) {
			var validobj = $(".uploadform").validate({
				errorClass: "uploadtoolError",
				errorPlacement: function (error, element) {
					var elem = $(element);
					error.insertAfter(element);
				},
				
				highlight: function (element,errorClass, validClass) {
					var elem = $(element);
					if (elem.hasClass("select2-offscreen")) {
						$("#s2id_" + elem.attr("id") + " ul").addClass(errorClass);
					} else {
						elem.addClass(errorClass);
					}
				},
				
				unhighlight: function (element, errorClass, validClass) {
					var elem = $(element);
					if (elem.hasClass("select2-offscreen")) {
						$("#s2id_" + elem.attr("id") + " ul").removeClass(errorClass);
						
					} else {
						elem.removeClass(errorClass);
					}
				}
			});
		
			$(document).on("change", ".select2-offscreen", function () {
				if (!$.isEmptyObject(validobj.submitted)) {
					validobj.form();
				}
			});
			
			$(".uploadform").on('submit', function(e) {
				var isValid = $(".uploadform").valid();
				if (isValid) {
					e.preventDefault();
					console.log("form submitted");
					
					let url = '/uploadtool/submit'
					
					let formData = new FormData();
					let galleryTable = document.getElementById("gallery");
					var docRows = [];
					for (var row of galleryTable.rows) {
						console.log("row id: " + row.id);
						let sourceSelectId = $('#source-' + row.id);
						let sourceVal = sourceSelectId.val();
						let targetSelectId = $('#target-' + row.id);
						let targetVals = targetSelectId.val();
						// let multiVals = multiSelect.select2().val();
						console.log("selected vals: s- " + sourceVal + " t- " + targetVals);
						let rowData = {
							'id': row.id,
							'source': sourceVal,
							'target': targetVals,
						}
						docRows.push(rowData);
					}
					console.log("docRows: " + docRows.length);
					console.log("fname: " + $('#fname').val());
					var postData = {
						'fname': $('#fname').val(),
						'lname': $('#lname').val(),
						'phone': $('#phone').val(),
						'email': $('#email').val(),
						'comments': $('#comments').val(),
						'docs': docRows,
					}
					
					var postJson = JSON.stringify(postData);
					// formData.append('docs', docRows);
					console.log("json: " + postJson);
					
					fetch(url, {
						method: 'POST',
						body: postJson,
					})
					.then(function(response){
						if (response.ok) {
							response.text().then(function(text) {
								document.getElementById("upload_tool").reset();
								let gallery = document.getElementById("gallery");
								for (var tr of gallery.rows) {
									tr.remove();
								}
								var successDiv = document.createElement("div");
								$('#hidden-result').append(successDiv);
								$(successDiv).append(text).dialog({
									dialogClass: "no-close",
									title: "Request Submitted",
									buttons: [
									{
										text: "OK",
										click: function() {
											$(this).dialog("close");
										}
									}
									]
								});
							});
						} else if (response.status == '500') {
							console.log("internal server error");
						}
					})
					.catch(() => {
						console.log("error submitting form");
					})
					
						
					
					// $('#gallery tr').each(function(){
						// let row = $(this);
						// console.log("row: " + row.attr("id"));
						// console.log("html: " + row.html);
						
						// $('this').find("
						// let multiSelect = row.cells.item(row.cells.length - 2);
						// let multiVals = multiSelect.select2().val();
						// console.log("selected vals: " + multiVals);
					// })
					
				}
			});
		}
	});
	
	let dropArea = document.getElementById('drop-area')

	//Prevent default drag behaviours
	;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
		dropArea.addEventListener(eventName, preventDefaults, false)
	})
	
	//Highlight drop area when item is dragged over it
	;['dragenter', 'dragover'].forEach(eventName => {
		dropArea.addEventListener(eventName, highlight, false)
	})

	;['dragleave', 'drop'].forEach(eventName => {
		dropArea.addEventListener(eventName, unhighlight, false)
	})

	function preventDefaults (e) {
		console.log("preventing defaults");
		e.preventDefault()
		e.stopPropagation()
	}
	
	
	function highlight(e) {
		dropArea.classList.add('highlight')
	}

	function unhighlight(e) {
		dropArea.classList.remove('highlight')
	}
	
	//Handle dropped files
	dropArea.addEventListener('drop', handleDrop, false)

	function handleDrop(e) {
		let dt = e.dataTransfer
		let files = dt.files

		handleFiles(files)
	}
	
	function handleFiles(files) {
		([...files]).forEach(uploadFile)
	}
	
	function uploadFile(file) {
		let url = '/uploadtool/upload_file'
		let formData = new FormData()

		formData.append('file_datas', file);
		formData.append('mimetype', file.type);
		console.log("filename: " + file.name + " type: " + file.type);
		fetch(url, {
			method: 'POST',
			body: formData
		})
		.then(function(response){ /* Done. Inform the user */ 
			// console.log("r: " + response.status);
			if (response.ok) {
				response.text().then(function(text){
					$('#gallery').append(text);
				});
			} else if (response.status == '415') {
				response.text().then(function(text){
					var errorDiv = document.createElement("div");
					$('#hidden-error').append(errorDiv);
					$(errorDiv).append(text).dialog({
						dialogClass: "no-close",
						title: "File Upload Failed",
						buttons: [
						{
							text: "OK",
							click: function() {
								$(this).dialog("close");
							}
						}
						]
					});
				});
			}
		})
		.catch(() => { 
			console.log("error");
			/* Error. Inform the user */ 
			})
	}
	
	//Add eventlistener to ensure files added through file input button are handled in the same way as those
	//that are dragged and dropped
	let fileElem = document.getElementById('fileElem')
	
	fileElem.addEventListener('change', function() {handleFiles(this.files);}, false)

	//set up observer to watch for elements added to gallery on successful file upload
	//and correctly initialise language selectors
	var gallery = document.getElementById('gallery');
	
	var mutationConfig = {childList: true};
	
	var callback = function(mutationList, observer) {
		for(var mutation of mutationList) {
			if (mutation.type == 'childList') {
				if (mutation.addedNodes.length > 0) {
					var childNodes = mutation.addedNodes;
					var addedRow = childNodes.item(1);
					var delCell = addedRow.cells.item(addedRow.cells.length - 1);
					var delBtn = delCell.children[0];
					delBtn.addEventListener('click', deleteRow, false);
					var targetSelectCell = addedRow.cells.item(addedRow.cells.length - 2);
					var targetSelect = targetSelectCell.children[0];
					
					$('#'+targetSelect.id).select2({
						placeholder: "--Target Language--",
						width: "300px",
					});
					$('#'+targetSelect.id).addClass("required");
					
				}
			}
		}
	};
	
	
	var observer = new MutationObserver(callback);
	
	observer.observe(gallery, mutationConfig);
	
	//function to delete row from gallery when user wishes to not upload that file
	function deleteRow(e) {
		console.log("delete clicked: " + e.target.parentNode.parentNode.id);
		var parentRow = e.target.parentNode.parentNode;
		let url = '/uploadtool/delete_file'
		let formData = new FormData();
		formData.append('record_id', parentRow.id);
		fetch(url, {
			method: 'POST',
			body: formData
		})
		.then(function(response){ /* Done. Inform the user */ 
			console.log("r: " + response.ok)
			if (response.ok) {
				parentRow.remove();
			}
		})
		.catch(() => { 
			console.log("error");
			/* Error. Inform the user */ 
			})
	}
	
	// var uploadForm = $('#upload-form');
	// $(uploadForm).submit(submitUpload);
	
	// let uploadForm = document.getElementById('upload-form');
	
	// uploadForm.addEventListener('submit', submitUpload, false);
	
	// function submitUpload(e) {
		// e.preventDefault();
		// e.stopPropagation()
		// console.log("form submitted: " + e);
	// }
	
});